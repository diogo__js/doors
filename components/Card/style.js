import styled from 'styled-components'

export const Container = styled.section`

    display: flex;
    width: 250px;
    justify-content: center;
    align-items: center;
    height: 250px;
    color: #fff;
    margin: 8px;
    font-size: 2rem;
    border-radius: 23px;
    box-shadow: 10px 5px 5px rgba(0,0,0,0.7);


    h2{
        font-weight: 'normal'
    }

`