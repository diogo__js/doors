import { Container, PortaStyle,  Numero, Macaneta, Chao } from './style'
import PortaModel from '../../model/porta'
import Presente from '../Presente'

interface PortaProps {
    value: PortaModel
    onChange: (novaPorta: PortaModel) => void
}

const Porta = (props: PortaProps) => {

    const porta = props.value
    const selecinada = porta.selecinada && !porta.aberta ? 'selecionada' : ''

    const alternarSelecao = e => props.onChange(porta.alternarSelecao())

    const abrir = e => {
        e.stopPropagation()
        props.onChange(porta.abrir())
    }

    

    const renderizarPorta = () => {
        return (
            <PortaStyle>
                <Macaneta onClick={abrir}> </Macaneta>
                <Numero> {porta.numero} </Numero>
            </PortaStyle>
        )
    }
    
    return(
        
        <Container onClick={alternarSelecao}>
            <div className={`estrutura ${selecinada}`}>

                {porta.fechada ? 
                    renderizarPorta() : porta.temPresente ? 
                    <Presente /> : false
                }

            </div>
            <Chao></Chao>
        </Container>
    )

}

export default Porta