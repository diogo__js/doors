import styled from 'styled-components'

export const Container = styled.section`

display: flex;
justify-content: space-around;
align-items: center;
flex-direction: column;
height: 100%;
color: black;

`


export const Value = styled.span`

font-size: 38px;

`


export const Text = styled.span`

font-size: 25px;

`

export const Botoes = styled.div`
display: flex;
justify-content: space-around;
width: 100%;

`

export const Botao = styled.button`


background: purple;

border: 0;
padding: 3px 34px;
border-radius: 10px;
font-size: 28px;
margin: 0 10px;
color: yellow;
font-weight: bold;
box-shadow: 5px 5px 5px rgba(0,0,0,0.5);
cursor: pointer;
border: 1px solid purple;

:hover{
    background: yellow;
    color: purple;
    transition: 1.9s;
    border: 1px solid black;
}

`