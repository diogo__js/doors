import { Container, Text, Value, Botoes, Botao } from './style'

interface EntradaNumericaProps {
    text: string
    value: number
    onChange: (newValue: number) => void
}

const EntradaNumerica = (props: EntradaNumericaProps) => {
    
    const encrementar = () => props.onChange(props.value + 1)
    const decrementar = () => props.onChange(props.value - 1)

    return (
        <Container>
            <Text> {props.text} </Text>
            <Value> {props.value} </Value>
            <Botoes>
                <Botao onClick={encrementar}>
                    +
                </Botao>
                <Botao onClick={decrementar}>
                    -
                </Botao>
            </Botoes>
        </Container>
    )

}

export default EntradaNumerica