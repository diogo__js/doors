import PortaModel from '../model/porta'

export function criarPortas(qtd: number, portaComPresente: number): PortaModel[] {
    
    return Array.from({length: qtd}, (_, i) => {

        const numero = i + 1
        const temPresente = numero === portaComPresente

        return new PortaModel(numero, temPresente)

    })
}

export function atualizarPortas(portas: PortaModel[], portaModificada: PortaModel): PortaModel[] {
    
    return portas.map(pAtual => {
       
        const igualAModificada = pAtual.numero === portaModificada.numero

        if(igualAModificada){
            return portaModificada
        } else{
            return portaModificada.aberta ? pAtual : pAtual.desselecionar()
        }
        
    })

}

