import { Container, Portas, Botoes } from './style'
import Link from 'next/link'
import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Porta from '../../../components/Porta'
import { atualizarPortas, criarPortas } from '../../../functions/portas'

const Jogo = () => {

    const [portas, setPortas] = useState([])
    const [valido, setValido] = useState(false)

    const router = useRouter()

    useEffect(() => {

      const portas = +router.query.portas
      const temPresente = +router.query.temPresente

      setPortas(criarPortas(portas, temPresente))

    }, [router?.query])


    useEffect(() => {

      const portas = +router.query.portas
      const temPresente = +router.query.temPresente

      const qtdPortasValidas = portas >= 3
      const temPresenteValido = temPresente >=1 && temPresente <= portas 

      setValido(qtdPortasValidas && temPresenteValido)

    }, [portas])


    const renderizarPortas = () => {

    return portas.map(p => {
      return <Porta key={p.numero} value={p} 
      onChange={novaPorta =>  {

        setPortas(atualizarPortas(portas, novaPorta))
        
      }}/>
    })

  }

    return (
        <Container>
            <Link href='/'>
                <Botoes> Reiniciar Jogo </Botoes>
            </Link>
         
            <Portas>
                {
                  valido ? renderizarPortas() : 
                  <h2> Valores inválidos </h2>
                }
            </Portas>
            
            <Link href='/'>
                <Botoes> Reiniciar Jogo </Botoes>
            </Link>
         
            
        </Container>     
    )

}

export default Jogo