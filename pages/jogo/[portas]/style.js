import styled from 'styled-components'

export const Container = styled.section`

display: flex;
flex-direction: column;
justify-content: space-around;
height: 100%;
align-items: center;
flex-wrap: wrap;
max-width: 1200px;
margin: 34px auto;


`

export const Portas = styled.div`

display: flex;
width: 100%;
justify-content: space-around;
flex-wrap: wrap;


`

export const Botoes = styled.button`

width: 187px;
height: 43px;
border-radius: 13px;
background-color: yellow;
color: black;
display: flex;
justify-content: center;
align-items: center;
font-weight: bold;
font-size: 1.1rem;
font-style: italic;
border: 1px solid black;
cursor: pointer;
margin: 38px;

:hover{
    background-color: black;
    color: yellow;
    transition: 1.9s;
}

`